<?php

require_once('animal.php');

class frog extends animal
{
        public $name;
        public $legs= 4;
        public $cold_blooded = "false";
   
        public function __construct($string)
        {
          $this->name = $string;  
        }
        public function jump(){
            echo "Jump Hop-hop";
        }
}
